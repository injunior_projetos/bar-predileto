<?php

// Adicionar arquivos .css e .js

function add_styles_and_scripts() {
    
	// all styles
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');

	// all scripts
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js');
	wp_enqueue_script( 'scroll', get_template_directory_uri() . '/assets/js/scroll.js');
    
}
add_action( 'wp_enqueue_scripts', 'add_styles_and_scripts' );

// Post Type Eventos

function custom_post_type_eventos() {
	register_post_type('eventos', array(
		'label' => 'Eventos',
        'description' => 'Eventos',
        'menu_position' =>  4,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'query_var' => true,
		'supports' => array('title', 'editor'),

		'labels' => array (
			'name' => 'Eventos',
			'singular_name' => 'Eventos',
			'menu_name' => 'Eventos',
			'add_new' => 'Eventos Novo',
			'add_new_item' => 'Adicionar Novo Eventos',
			'edit' => 'Editar',
			'edit_item' => 'Editar Eventos',
			'new_item' => 'Novo Eventos',
			'view' => 'Ver Eventos',
			'view_item' => 'Ver Eventos',
			'search_items' => 'Procurar Eventos',
			'not_found' => 'Nenhum Eventos Encontrado',
			'not_found_in_trash' => 'Nenhum Eventos Encontrado no Lixo',
		)
	));
}
add_action('init', 'custom_post_type_eventos');


?>