<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style>
.loader {
    font-size: 10px;
    margin: 50px auto;
    text-indent: -9999em;
    width: 11em;
    height: 11em;
    border-radius: 50%;
    background: #dc3545;
    background: -moz-linear-gradient(left, #dc3545 10%, rgba(255, 255, 255, 0) 42%);
    background: -webkit-linear-gradient(left, #dc3545, rgba(255, 255, 255, 0) 42%);
    background: -o-linear-gradient(left, #dc3545 10%, rgba(255, 255, 255, 0) 42%);
    background: -ms-linear-gradient(left, #dc3545 10%, rgba(255, 255, 255, 0) 42%);
    background: linear-gradient(to right, #dc3545 10%, rgba(255, 255, 255, 0) 42%);
    position: relative;
    -webkit-animation: load3 1.4s infinite linear;
    animation: load3 1.4s infinite linear;
    -webkit-transform: translateZ(0);
    -ms-transform: translateZ(0);
    transform: translateZ(0);
  }
  .loader:before {
    width: 50%;
    height: 50%;
    background: #dc3545;
    border-radius: 100% 0 0 0;
    position: absolute;
    top: 0;
    left: 0;
    content: '';
  }
  .loader:after {
    background: #fff;
    width: 75%;
    height: 75%;
    border-radius: 50%;
    content: '';
    margin: auto;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
  }
  @-webkit-keyframes load3 {
    0% {
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  @keyframes load3 {
    0% {
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
</style>
<div class="loader">Loading...</div>
<?php

// Trocar os valores abaixo
$email_destinatario = 'financeiro@barpredileto.com.br';
$assunto = 'Formulario Bar Predileto';
$url = '/';

// Valores do usuario -> busca pelo ID
$nome = $_POST['nome-cliente'];
$email = $_POST['email-cliente'];
$mensagem = $_POST['mensagem-cliente'];

// Verifica se nome é válido
if(empty($nome) || !preg_match("/^[a-zA-Z'-]/", $nome)) {
    echo "<script> alert('Nome inválido.'); </script>";
    echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=" . $url . "'>";
} else {
    // Verifica se email é válido
    if(!filter_var( $email, FILTER_VALIDATE_EMAIL )) {
        echo "<script> alert('E-mail inválido.'); </script>";
        echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=" . $url . "'>";
    } else {
        // Verifica se mensagem é válida
        if(empty($mensagem) || !preg_match("/^[a-zA-Z'-]/", $mensagem)) {
            echo "<script> alert('Mensagem inválida.'); </script>";
            echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=" . $url . "'>";  
        } else {

            $body = "Formulário enviado:\n\nNome: $nome \nE-mail: $email \nMensagem: $mensagem \n";

            // Configurando o PHPMailer

            require ('./PHPMailer/PHPMailerAutoload.php');

            $mail = new PHPMailer;
            $mail->CharSet = 'UTF-8';
            $mail->WordWrap = 70;
            $mail->addAddress($email_destinatario);
            $mail->From = $email;
            $mail->FromName = $nome;
            $mail->AddReplyTo($email, $nome);
            $mail->Subject = $assunto;
            $mail->Body = $body;

            if(!$mail->send()) {
                echo "<script> setTimeout(() => { swal('Erro!', 'Falha ao enviar o Formulário!', 'error').then((value) => { window.location.replace('/') }) }, 3000);  </script>";
            } else {
                echo "<script> setTimeout(() => { swal('Feito!', 'Formulário enviado com sucesso!', 'success').then((value) => { window.location.replace('/') }) }, 3000);  </script>";
            }
        }
    }
}
?>